/**************************************************************************
    pam_exec.c  Execute Script Pam Modules 

    Copyright (C) 2004 Hal D. Duston <hald@kc.rr.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Based off of:
    $Id: pam_exec.c,v 1.2 2006/06/28 15:18:28 hald Exp $
  
    Revision history:

        1.00    2004, July 01     Hal D. Duston <hald@kc.rr.com>
                Initial releases
        2.00    2005, January 05  Hal D. Duston <hald@kc.rr.com>
                Substantial reworking to support the full pam functionality.
 
************************************************************************/

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <paths.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

/*
 * here, we make a definition for the externally accessible which_exection
 * in this file (this definition is required for static a module
 * but strongly encouraged generally) it is used to instruct the
 * modules include file to define the which_exection prototypes.
 */

#define PAM_SM_AUTH
#define PAM_SM_ACCOUNT
#define PAM_SM_SESSION
#define PAM_SM_PASSWORD

#include <security/pam_modules.h>

#define countof(x) (sizeof(x)/sizeof((x)[0]))

#define PAM_EXEC_AUTHENTICATE  1
#define PAM_EXEC_SETCRED       3
#define PAM_EXEC_ACCOUNT       2
#define PAM_EXEC_OPEN_SESSION  4
#define PAM_EXEC_CLOSE_SESSION 5
#define PAM_EXEC_CHAUTHTOK     6
#define PAM_EXEC_MAX           PAM_EXEC_CHAUTHTOK

static const char *pam_names[] =
{
	/* These _MUST_ (off by one) match the PAM_EXEC #defines.  */
	"authenticate", "setcred", "account", "open_session", "close_session", "chauthtok",
};

static
int _pam_exec_dispatch(pam_handle_t *pamh, int flags, int argc, const char **argv, int which_exec);

PAM_EXTERN
int pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc,
			const char **argv)
{
	return _pam_exec_dispatch(pamh, flags, argc, argv, PAM_EXEC_AUTHENTICATE);
}

PAM_EXTERN
int pam_sm_setcred(pam_handle_t *pamh, int flags, int argc,
		   const char **argv)
{
	return _pam_exec_dispatch(pamh, flags, argc, argv, PAM_EXEC_SETCRED);
}

PAM_EXTERN
int pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc,
		     const char **argv)
{
	return _pam_exec_dispatch(pamh, flags, argc, argv, PAM_EXEC_ACCOUNT);
}

PAM_EXTERN
int pam_sm_open_session(pam_handle_t *pamh, int flags, int argc,
			const char **argv)
{
	return _pam_exec_dispatch(pamh, flags, argc, argv, PAM_EXEC_OPEN_SESSION);
}

PAM_EXTERN
int pam_sm_close_session(pam_handle_t *pamh, int flags, int argc,
			 const char **argv)
{
	return _pam_exec_dispatch(pamh, flags, argc, argv, PAM_EXEC_CLOSE_SESSION);
}

PAM_EXTERN
int pam_sm_chauthtok(pam_handle_t *pamh, int flags, int argc,
		     const char **argv)
{
	return _pam_exec_dispatch(pamh, flags, argc, argv, PAM_EXEC_OPEN_SESSION);
}

/* logging */
static void _pam_log(int err, const char *format, ...)
{
    va_list args;
                                                                                                                             
    va_start(args, format);
    openlog("pam_exec", LOG_CONS | LOG_PID, LOG_AUTH);
    vsyslog(err, format, args);
    va_end(args);
    closelog();
}
                                                                                                                             
static
int _pam_exec_dispatch(pam_handle_t *pamh, int flags, int argc, const char **argv, int which_exec)
{
	/* The following two arrays _MUST_ match exactly.  */
	static const int items[] = {
		PAM_SERVICE,
		PAM_USER,
		PAM_TTY,
		PAM_RHOST,
		PAM_AUTHTOK,
		PAM_OLDAUTHTOK,
		PAM_RUSER,
		PAM_USER_PROMPT,
	};
	static const char *item_names[] = {
		"SERVICE",
		"USER",
		"TTY",
		"RHOST",
		"AUTHTOK",
		"OLDAUTHTOK",
		"RUSER",
		"USER_PROMPT",
	};
	int retval = PAM_IGNORE;
	int status = 0;
	const char *item;
	char arg[2][BUFSIZ];
	char *argp[3];
	char env[countof(items)][BUFSIZ];
	char *envp[countof(items) + 1];
	pid_t child;
	int i;
	int j;

	argp[0] = NULL;
	/* Step through the arguments.  */
	for (i = 0; i < argc; ++i) {
		/* Is this the option for the function we were called through?  */
		if(!strncmp(argv[i],
			    pam_names[which_exec - 1],
			    strlen(pam_names[which_exec - 1]))) {
			argp[0] = arg[0];
			strncpy(arg[0],
				argv[i] + strlen(pam_names[which_exec - 1]) + 1,
				sizeof(arg[0]));
			break;
		}
	}

	/* Is there a script/executable to run for this function?  */
	if (argp[0] != NULL) {

		/* Setup the arguments for the script/executable.  */
		argp[1] = arg[1];
		strcpy(arg[1], pam_names[which_exec - 1]);
		argp[2] = NULL;

		/* Setup the environment for the script/executable.  */
		for (i = 0, j = 0; i < countof(items); ++i) {
			if (pam_get_item(pamh,
					 items[i],
					 (const void **)&item) == PAM_SUCCESS
			    && item != NULL
			    && *item != '\0') {
				envp[j] = env[j];
				sprintf(env[j], "%s=%s", item_names[i], item);
				++j;
			}
		}
		envp[j] = NULL;

		if ((child = fork()) == 0) {
			/* CHILD PROCESS */

			int fdlimit;
			int nullfd;

			/* Set stdin, stdout, and stderr to the null device.  */
			nullfd = open(_PATH_DEVNULL, O_RDWR);
			dup2(nullfd, STDIN_FILENO);
			dup2(nullfd, STDOUT_FILENO);
			dup2(nullfd, STDERR_FILENO);

			/* Close all the other files.  */
			for (fdlimit = sysconf(_SC_OPEN_MAX) - 1;
			    fdlimit > STDERR_FILENO;
			    --fdlimit) {
				close(fdlimit);
			}

			/* Set process group leader.  */
			setpgid(0, getpid());

			/* Run the script/executable.  */
			execve(argp[0], argp, envp);

			_pam_log(LOG_ERR, "\"%s\" : %s", argp[0], strerror(errno));

			/* If we get here the exec() failed.  */
			_exit(PAM_SYSTEM_ERR);
		}

		else if (child > 0) {
			/* PARENT PROCESS */

			waitpid(child, &status, 0);
			if (WIFEXITED(status)) {
				retval = WEXITSTATUS(status);
			}
			else {
				retval = PAM_SYSTEM_ERR;
			}
		}
		else {
			/* FORK FAILED */

			_pam_log(LOG_ERR, "%s", strerror(errno));
			retval = PAM_SYSTEM_ERR;
		}
	}
	else {
		retval = PAM_SYSTEM_ERR;
	}
	return retval;
}

#ifdef PAM_STATIC

/* static module data */

struct pam_module _pam_exec_modstruct = {
	"pam_exec",
	pam_sm_authenticate,
	pam_sm_setcred,
	pam_sm_acct_mgmt,
	pam_sm_open_session,
	pam_sm_close_session,
	pam_sm_chauthtok,
};

#endif

/* end of module definition */

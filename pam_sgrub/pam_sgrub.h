/*
 *	pam_sgrub.c
 *	PAM module for local authentication
 *	GPL
 * */

#ifndef PAM_SGRUB_H_
#define PAM_SGRUB_H_

#define PAM_SM_AUTH
#define PROC_PWD_PATH	"/proc/cmdline"
#define DEVNULL_PATH	"dev/null"
#define SYS_PWD_PATH	"/tmp/system_update"
#define PROC_PWD_STR	"pwd="

#define BUF_LENGHT		4048
#define PWD_LENGHT		40
//Read to /proc/cmdline restricted to 256 bits on x86 platform
#define MAX_READ 		256
#define PATH_MAX		4096
#define PAM_EXEC_AUTHENTICATE  1


#endif /*PAM_SGRUB_H_*/

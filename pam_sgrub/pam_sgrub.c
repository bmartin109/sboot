/*      This file contains functions and utilities for the Secured GRUB project
        at http://sboot.linbox.org/. The SHA1-implementation has been written by
        Martin Brice <martin.brice@edge-it.fr> and tested according to FIPS-180.
        For reuasage of the SHA1-implementation, please contact the original author.

	Furthermore, this file contains the GRUB implementation of "calculate_sha1".
	For details, read the README-file or contact the author Martin Brice
	<martin.brice@edge-it.fr>
*/

#include <security/pam_modules.h>
#include <security/_pam_macros.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <mntent.h>
#include <stdbool.h>
#include "pam_sgrub.h"

#define countof(x) (sizeof(x)/sizeof((x)[0]))

static const char *pam_names[] = {
/* These _MUST_ (off by one) match the PAM_EXEC #defines.  */
"authenticate", "setcred", "account", "open_session", "close_session",
		"chauthtok", 
};

/* logging */
static void _pam_log(int err, const char *format, ...) {
	va_list args;
	va_start(args, format);
	openlog("pam_sgrub", LOG_CONS | LOG_PID, LOG_AUTH);
	vsyslog(err, format, args);
	va_end(args);
	closelog();
}

int pam_exec(pam_handle_t *pamh, int flags, int argc, const char **argv,
		int which_exec) {
	/* The following two arrays _MUST_ match exactly.  */
	static const int items[] = { PAM_SERVICE, PAM_USER, PAM_TTY, PAM_RHOST,
			PAM_AUTHTOK, PAM_OLDAUTHTOK, PAM_RUSER, PAM_USER_PROMPT, 
};
static const char *item_names[] = {
	"SERVICE",
	"USER",
	"TTY",
	"RHOST",
	"AUTHTOK",
	"OLDAUTHTOK",
	"RUSER",
	"USER_PROMPT",
};
int retval = PAM_SUCCESS;
int status = 0;
const char *item;
char arg[2][BUFSIZ];
char *argp[3];
char env[countof(items)][BUFSIZ];
char *envp[countof(items) + 1];
pid_t child;
int i;
int j;

argp[0] = NULL;
/* Step through the arguments.  */
for (i = 0; i < argc; ++i) {
	/* Is this the option for the function we were called through?  */
	if(!strncmp(argv[i],
					pam_names[which_exec - 1],
					strlen(pam_names[which_exec - 1]))) {
		argp[0] = arg[0];
		strncpy(arg[0],
				argv[i] + strlen(pam_names[which_exec - 1]) + 1,
				sizeof(arg[0]));
		break;
	}
}

/* Is there a script/executable to run for this function?  */
if (argp[0] != NULL) {

	/* Setup the arguments for the script/executable.  */
	argp[1] = arg[1];
	strcpy(arg[1], pam_names[which_exec - 1]);
	argp[2] = NULL;

	/* Setup the environment for the script/executable.  */
	for (i = 0, j = 0; i < countof(items); ++i) {
		if (pam_get_item(pamh,
						items[i],
						(const void **)&item) == PAM_SUCCESS
				&& item != NULL
				&& *item != '\0') {
			envp[j] = env[j];
			sprintf(env[j], "%s=%s", item_names[i], item);
			++j;
		}
	}
	envp[j] = NULL;

	if ((child = fork()) == 0) {
		/* CHILD PROCESS */

		int fdlimit;
		int nullfd;

		/* Set stdin, stdout, and stderr to the null device.  */
		nullfd = open(DEVNULL_PATH, O_RDWR);
		dup2(nullfd, STDIN_FILENO);
		dup2(nullfd, STDOUT_FILENO);
		dup2(nullfd, STDERR_FILENO);

		/* Close all the other files.  */
		for (fdlimit = sysconf(_SC_OPEN_MAX) - 1;
				fdlimit > STDERR_FILENO;
				--fdlimit) {
			close(fdlimit);
		}

		/* Set process group leader.  */
		setpgid(0, getpid());

		/* Run the script/executable.  */
		execve(argp[0], argp, envp);

		_pam_log(LOG_ERR, "\"%s\" : %s", argp[0], strerror(errno));

		/* If we get here the exec() failed.  */
		_exit(PAM_SYSTEM_ERR);
	}

	else if (child > 0) {
		/* PARENT PROCESS */

		waitpid(child, &status, 0);
		if (WIFEXITED(status)) {
			retval = WEXITSTATUS(status);
		}
		else {
			retval = PAM_SYSTEM_ERR;
		}
	}
	else {
		/* FORK FAILED */

		_pam_log(LOG_ERR, "%s", strerror(errno));
		retval = PAM_SYSTEM_ERR;
	}
}
else {
	retval = PAM_SYSTEM_ERR;
}
return retval;
}

int already_mounted(const char *mnt_path, const char *dev_path) {
	char dev[PATH_MAX+1] = { }, real_mpt[PATH_MAX+1];
	struct mntent *mtab_record;
	bool mounted = false;
	FILE *mtab;

	//assert(config_valid(config));

	if ((mtab = setmntent("/etc/mtab", "r")) == NULL) {
		syslog(LOG_WARNING, "Could not open /etc/mtab\n");
		return -1;
	}

	if (realpath(mnt_path, real_mpt) == NULL) {
		syslog(LOG_WARNING, "Can't get realpath of volume %s: %s\n", mnt_path,
				strerror(errno));
		strncpy(real_mpt, mnt_path, sizeof(real_mpt)-1);
		real_mpt[sizeof(real_mpt)-1] = '\0';
	} else {
		real_mpt[sizeof(real_mpt)-1] = '\0';
	}

	while ((mtab_record = getmntent(mtab)) != NULL) {
		const char *fsname = mtab_record->mnt_fsname;
		const char *fstype = mtab_record->mnt_type;
		const char *fspt = mtab_record->mnt_dir;
		int (*xcmp)(const char *, const char *);

		xcmp = (strcmp(fstype, "smbfs") == 0 || strcmp(fstype, "cifs") == 0
				|| strcmp(fstype, "ncpfs") == 0) ? strcasecmp : strcmp;

		syslog(LOG_WARNING,
				"Question: Device path \"%s\" Type \"%s\" Directory  \"%s\"\n",
				fsname, fstype, fspt);

		if (xcmp(fsname, dev_path) == 0 && (strcmp(fspt, mnt_path) == 0
				|| strcmp(fspt, real_mpt) == 0)) {
			mounted = true;
			break;
		}
	}

	endmntent(mtab);
	return mounted;
}

PAM_EXTERN int pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc,
		const char **argv) {
	int ret = PAM_SUCCESS;
	FILE *fd_proc;
	char *authtok = NULL;
	char *tmp = NULL;
	char *cmdline = NULL;
	char *password = NULL;
	int i;
	//assert(pamh != NULL);

	// Get password from PAM system
	ret = pam_get_item(pamh, PAM_AUTHTOK, (const void **) &password);
	if (ret != PAM_SUCCESS || password == NULL) {
		syslog(LOG_WARNING,
				"pam_sgrub: Could not get password from PAM system\n");
		return PAM_SYSTEM_ERR;
	} else {
		struct stat stFileInfo;
		int fd_test = stat(SYS_PWD_PATH, &stFileInfo);
		if (fd_test != 0) {
			fd_proc = fopen(PROC_PWD_PATH, "r");
			if (!fd_proc) {
				syslog(LOG_INFO, "pam_sgrub: Error opening device %s\n", 
				PROC_PWD_PATH);
				fclose(fd_proc);
				return PAM_SYSTEM_ERR;
			}

			cmdline = malloc(sizeof(char) * MAX_READ);

			if (!cmdline) {
				syslog(LOG_WARNING,
						"pam_sgrub: Unable to malloc space for %s\n", 
						PROC_PWD_PATH);
				fclose(fd_proc);
				return PAM_SYSTEM_ERR;
			}

			cmdline = fgets(cmdline, MAX_READ, fd_proc);
			authtok = strstr(cmdline, PROC_PWD_STR);

			strtok(authtok, "=");
			authtok = strtok(NULL, " ");

			if (authtok==NULL) {
				syslog(LOG_WARNING, "pam_sgrub: Unable to find sGrub password");
				free(cmdline);
				fclose(fd_proc);
				return PAM_AUTHINFO_UNAVAIL;
			}

			// Concatenate password & sGrub password
			//result = strcat(tmp, (const char *) );

			tmp = malloc(sizeof(char) * (PWD_LENGHT+strlen(password)+1));

			for (i=0; i < PWD_LENGHT+strlen(password)+1; i++)
				tmp[i]='\0';
			memcpy(tmp, (const char *)authtok, PWD_LENGHT);
			for (i=0; i<strlen(password) ; i++)
				tmp[PWD_LENGHT+i]=password[i];

			syslog(LOG_INFO, "pam_sgrub: New Password: %s\n", tmp);

			/* pam_set_item() copies to PAM-internal memory */
			ret = pam_set_item(pamh, PAM_AUTHTOK, tmp);
			if (ret != PAM_SUCCESS) {
				syslog(LOG_WARNING,
						"pam_sgrub: Error trying to export password\n");
				free(cmdline);
				fclose(fd_proc);
				return PAM_SYSTEM_ERR;
			}

			free(cmdline);
			fclose(fd_proc);

		} else {
			int fd_test = remove(SYS_PWD_PATH);
			if (fd_test != 0) {
				syslog(LOG_WARNING,
						"pam_sgrub: Error trying to exit System Update Mode\n");
				return PAM_SYSTEM_ERR;
			} else {
				char *login = NULL;
				// Get login from PAM system
				ret = pam_get_item(pamh, PAM_USER_PROMPT,
						(const void **) &login);
				if (ret != PAM_SUCCESS || login == NULL) {
					syslog(LOG_WARNING,
							"pam_sgrub: Could not get login name from PAM system\n");
					return PAM_SYSTEM_ERR;
				}

				//TODO tester les arguments et pam_session pour verifier dans mtab l'existence du montage pour arreter la session				
				return pam_exec(pamh, flags, argc, argv, PAM_EXEC_AUTHENTICATE);
			}
		}
	}

	return PAM_SUCCESS;
}

PAM_EXTERN
int pam_sm_open_session(pam_handle_t *pamh, int flags, int argc,
		const char **argv) {
	int i = already_mounted("/home/b.img", "/dev/loop");
	return PAM_SUCCESS;
}

PAM_EXTERN
int pam_sm_close_session(pam_handle_t *pamh, int flags, int argc,
		const char **argv) {
	return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_setcred(pam_handle_t *pamh, int flags, int argc,
		const char **argv) {
	return PAM_SUCCESS;
}

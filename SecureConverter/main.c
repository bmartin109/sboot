#include "parser.h"
#include "param.h"

int main(int argc, char *argv[]) {
	int in = 0;
	int out = 0;
	char *input= NULL;
	char *output= NULL;
	int c;
	opterr = 0;
	
	if (argc == 1 ) {
		printf("Usage: %s filename [-l {filename_list}] [-o {filename_list}]\n", argv[0]);
		return 1;
	}

	while ((c = getopt(argc, argv, "hl:o:")) != -1) {
		switch (c) {
		case 'l':
			input = optarg;
			in = 1;
			break;
		case 'o':
			output = optarg;
			out = 1;
			break;
		case 'h':
			fprintf(stderr, "Usage: %s filename [-l {filename_list}] [-o {filename_list}]\n", argv[0]);
			break;
		case '?':
			if ((optopt == 'l') || (optopt == 'o') )
				fprintf(stderr, "Option -%c requires an argument.\n", optopt);
			else 
				fprintf(stderr, "Usage: %s filename [-l {filename_list}] [-o {filename_list}]\n", argv[0]);
			return 1;
		default:
			abort();
		}
	}
	
	if (!in) {
		input = argv[optind];
	}

	if (parse_init(out, output)) {
		fprintf(stderr, "Error: Initialization Failed\n");
		return 1;
	}

	if (parse_process(in, input)) {
		fprintf(stderr, "Error: Parse Failure\n");
		parse_finish();
		return 1;
	}

	if (parse_finish()) {
		fprintf(stderr, "Error: Ending Process Failure\n");
		return 1;
	}

	return 0;
}


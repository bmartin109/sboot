#include "param.h"

#ifndef CHECK_LIST_H_
#define CHECK_LIST_H_

int		parse_init(int i, char *arg);
int		parse_process(int i, char *arg);
int		parse_finish();


#endif /*CHECK_LIST_H_*/

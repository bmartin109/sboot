#ifndef PARAM_H_
#define PARAM_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h> 

#define MAX_MOUNT		256
#define LINE_BUFFER_SIZE 1024
#define MNT_CONFIG		"/etc/mtab"
#define GRUB_MAP 		"/boot/grub/device.map"

#endif /*PARAM_H_*/

#include "parser.h"

static char tab[MAX_MOUNT][LINE_BUFFER_SIZE];
static FILE *fd_mtab, *fd_map, *fd_result;
static int lenght;

int dev_find(char *name, char * res) {
	int i=0, error=0;
	char arg1[LINE_BUFFER_SIZE], arg2[LINE_BUFFER_SIZE];
	char *tmp=NULL;

	while (i < lenght) {
		memset(arg1, '\0', LINE_BUFFER_SIZE);
		memset(arg2, '\0', LINE_BUFFER_SIZE);
		if (sscanf(&tab[i], "%s %s", arg1, arg2) < 1) {
			error++;
			break;
		}

		if (!strncmp((char*) name, arg2, strlen(arg2)))
			break;
		i++;
	}
	if (error)
		return 1;

	if (strlen(arg2) != 1)
		tmp = &name[strlen(arg2)];
	else
		tmp = &name[0];

	if (!tmp) {
		perror("Error in creating new file");
		return 1;
	}

	tmp = strcat(arg1, tmp);

	strcpy(res, tmp);

	return 0;
}

int gmap_conv(char *name, char *res) {
	char devpath[9];
	char *buf;
	char car;
	int not_found = 1, i = 0;
	char arg1[LINE_BUFFER_SIZE], tmp[LINE_BUFFER_SIZE];

	memset(devpath, '\0', sizeof(devpath));
	strncpy(devpath, name, sizeof(devpath)-1);
	if (!devpath) {
		perror("Error in conversion type");
		return 1;
	}

	memset(arg1, '\0', LINE_BUFFER_SIZE);
	memset(tmp, '\0', LINE_BUFFER_SIZE);
	buf = (char *) malloc(LINE_BUFFER_SIZE * sizeof(char));

	fseek(fd_map, 0, SEEK_SET);

	while (fgets(buf, LINE_BUFFER_SIZE, fd_map)) {
		if (buf[0] == '#' || buf[0] == '\n')
			continue;
		if (strstr(buf, devpath) != NULL) {
			not_found=0;
			sscanf(buf, "%s", arg1);
		}
	}

	if (not_found) {
		perror("Device not found!");
		return 1;
	}

	//Conversion into Grub Naming
	memset(devpath, '\0', sizeof(devpath));
	for (i=0; i<4; i++)
		devpath[i] = arg1[i];

	car = name[8];

	sprintf(tmp, "%s,%d)%s\n", devpath, (unsigned int) (atoi(&name[8])-1), &name[9]);
	strcpy(res, tmp);
	return 0;
}

int dev_cmp(const void *tab1, const void *tab2) {
	char arg1[LINE_BUFFER_SIZE], arg2[LINE_BUFFER_SIZE], arg2_2[LINE_BUFFER_SIZE];

	if (sscanf(tab1, "%s %s", arg1, arg2) < 2) {
		return 1;
	}

	if (sscanf(tab2, "%s %s", arg1, arg2_2) < 2) {
		return 1;
	}

	if (strlen(arg2) < strlen(arg2_2))
		return 1;
	else
		return -1;
}

int parse_init(int j, char *arg) {
	char *buf;
	int i=0, error=0;

	if (j & (arg != NULL)) {
		fd_result = fopen(arg, "w");
		if (!fd_result) {
			fprintf(stderr,"%s : ", arg);
			perror("");
			error++;
			return 1;
		}
	} else
		fd_result = NULL;

	fd_mtab = fopen(MNT_CONFIG, "r");
	if (!fd_mtab) {
		perror("Error in opening '/etc/mtab'");
		error++;
		return 1;
	}

	fd_map = fopen(GRUB_MAP, "r");
	if (!fd_map) {
		perror("Error in opening '/boot/grub/device.map'");
		error++;
		return 1;
	}

	memset(tab, '\0', MAX_MOUNT*LINE_BUFFER_SIZE);
	buf = (char *) malloc(LINE_BUFFER_SIZE * sizeof(char));

	while (fgets(buf, LINE_BUFFER_SIZE, fd_mtab)) {
		if (buf[0] == '#' || buf[0] == '\n')
			continue;
		if (!strncmp(buf, "/dev/", 5)) {
			if (strncpy(&tab[i], buf, strlen(buf)-1) == NULL) {
				perror("Error in sorting mounting point");
				error++;
				break;
			}
			i++;
		}
	}

	if (error)
		return 1;

	lenght = i;

	qsort(tab, lenght, LINE_BUFFER_SIZE, dev_cmp);

	return 0;
}

int parse_finish() {
	if (fd_result != NULL) {
		fflush(fd_result);
		fclose(fd_result);
	}
	fclose(fd_mtab);
	fclose(fd_map);

	return 0;
}

int parse_process(int i, char *arg) {
	char *buf= NULL;
	FILE *fd_conf= NULL;
	int error = 0;
	char devname[LINE_BUFFER_SIZE];
	char grubname[LINE_BUFFER_SIZE];
	char check_file_data[LINE_BUFFER_SIZE];
	int fd_test;
	struct stat stFileInfo;

	buf = malloc(LINE_BUFFER_SIZE * sizeof(char));

	if (i & (arg != NULL)) {
		fd_conf = fopen(arg, "r");
		if (!fd_conf) {
			perror("");
			error++;
			return 1;
		}
	}

	if (i & (fd_conf != NULL)) {

		while (fgets(buf, LINE_BUFFER_SIZE, fd_conf)) {

			if (buf[0] == '#' || buf[0] == '\n')
				continue;

			memset(devname, '\0', sizeof(char)*LINE_BUFFER_SIZE);
			memset(check_file_data, '\0', sizeof(char)*LINE_BUFFER_SIZE);
			memset(grubname, '\0', sizeof(char)*LINE_BUFFER_SIZE);

			if (strncpy(check_file_data, buf, strlen(buf)-1) == NULL) {
				perror("Wrong file format");
				error++;
				break;
			}

			fd_test = stat(check_file_data, &stFileInfo);
			if (fd_test != 0) {
				fprintf(stderr,"%s : ", check_file_data);
				perror("Missing or not existing File");
				close(fd_test);
				continue;
			}
			close(fd_test);

			if (dev_find(check_file_data, devname)) {
				perror("Couldn't find the partition");
				continue;
			}

			if (gmap_conv(devname, grubname)) {
				perror("Couldn't find the partition");
				continue;
			}

			if (fd_result != NULL) {
				if (!fputs(grubname, fd_result)) {
					perror("");
					continue;
				}
			} else {
				printf("%s", grubname);
			}
		}

		if (error) {
			free(buf);
			fclose(fd_conf);
			return 1;
		}

		free(buf);
	} else if (!i & (arg != NULL)) {
		fd_test = open(arg, O_RDONLY);
		if (fd_test == -1) {
			fprintf(stderr,"%s : ", check_file_data);
			perror("");
			return 1;
		}
		close(fd_test);

		if (dev_find(arg, devname)) {
			perror("Couldn't find the partition");
			error++;
		}

		if (gmap_conv(devname, grubname)) {
			perror("Couldn't find the partition");
			error++;
		}

		if (fd_result != NULL) {
			if (!fputs(grubname, fd_result)) {
				perror("");
				error++;
			}

		} else {
			printf("%s", grubname);
		}
	}
	
	if (fd_conf != NULL)
		fclose(fd_conf);

	if (error) return 1;
	
	return 0;
}


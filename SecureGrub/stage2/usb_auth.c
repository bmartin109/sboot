#include "shared.h"
#include "linux-asm-io.h"
#include "pc_slice.h"

#define  PCIBIOS_SUCCESSFUL                0x00

/*
 * Functions for accessing PCI configuration space with type 1 accesses
 */

#define CONFIG_CMD(bus, device_fn, where)   (0x80000000 | (bus << 16) | (device_fn << 8) | (where & ~3))

int pcibios_read_config_byte(unsigned int bus, unsigned int device_fn, unsigned int where,
		unsigned char *value) {
	outl(CONFIG_CMD(bus,device_fn,where), 0xCF8);
	*value = inb(0xCFC + (where&3));
	return PCIBIOS_SUCCESSFUL;
}

int pcibios_read_config_word(unsigned int bus, unsigned int device_fn, unsigned int where,
		unsigned short *value) {
	outl(CONFIG_CMD(bus,device_fn,where), 0xCF8);
	*value = inw(0xCFC + (where&2));
	return PCIBIOS_SUCCESSFUL;
}

int pcibios_read_config_dword(unsigned int bus, unsigned int device_fn, unsigned int where,
		unsigned int *value) {
	outl(CONFIG_CMD(bus,device_fn,where), 0xCF8);
	*value = inl(0xCFC);
	return PCIBIOS_SUCCESSFUL;
}

int pcibios_write_config_byte(unsigned int bus, unsigned int device_fn, unsigned int where,
		unsigned char value) {
	outl(CONFIG_CMD(bus,device_fn,where), 0xCF8);
	outb(value, 0xCFC + (where&3));
	return PCIBIOS_SUCCESSFUL;
}

int pcibios_write_config_word(unsigned int bus, unsigned int device_fn, unsigned int where,
		unsigned short value) {
	outl(CONFIG_CMD(bus,device_fn,where), 0xCF8);
	outw(value, 0xCFC + (where&2));
	return PCIBIOS_SUCCESSFUL;
}

int pcibios_write_config_dword(unsigned int bus, unsigned int device_fn, unsigned int where,
		unsigned int value) {
	outl(CONFIG_CMD(bus,device_fn,where), 0xCF8);
	outl(value, 0xCFC);
	return PCIBIOS_SUCCESSFUL;
}

#undef CONFIG_CMD

int pci_scan_bus(sha1_context *sha1_cxt) {
	unsigned int devfn, l, bus, buses=256;
	unsigned char hdr_type = 0;
	unsigned int vendor, device;
	unsigned int membase, ioaddr, romaddr;
	int i, reg;
	unsigned int pci_ioaddr = 0;
	int hash_error=0;
	unsigned int devfn_std=0, devfn_std_1=0, devfn_num=0;
	char namebuf[20];

	// parameter check
	if (sha1_cxt == NULL) {
		printf("\nSGRUB: Error Initialization Context\n");
		return 0;
	}

	for (i=0; i<20; i++)
		namebuf[i]='\0';

	for (bus =  0 ; bus < buses ; bus++) {
		for (devfn = 0 ; devfn < 0xff ; devfn++) {

			if (PCI_FUNC(devfn) == 0)
				pcibios_read_config_byte(bus, devfn, PCI_HEADER_TYPE, &hdr_type);

	//		if (!(hdr_type & 0x80)) /* not a multi-function device */
		//		continue;

			pcibios_read_config_dword(bus, devfn, PCI_VENDOR_ID, &l);

			/* some broken boards return 0 if a slot is empty: */
			if (l == 0xffffffff || l == 0x00000000) {
				hdr_type = 0;
				continue;
			}
			vendor = l & 0xffff;
			device = (l >> 16) & 0xffff;
			devfn_std = ((devfn >> 3) & 0xf0) >> 4;
			devfn_std_1 = (devfn >> 3) & 0xf;
			devfn_num = devfn & 0x7;

			sprintf(namebuf, "%x%x%x%x%x%x", bus, devfn_std, devfn_std_1, devfn_num, vendor, device);			

			if (sha1_update(sha1_cxt, (unsigned char *) namebuf, strlen(namebuf))) {
				printf("\nSGRUB Error: SHA1 Hash Update\n");
				hash_error++;
				break;
			}

			/*			for (reg = PCI_BASE_ADDRESS_0; reg <= PCI_BASE_ADDRESS_5; reg += 4) {
			 pcibios_read_config_dword(bus, devfn, reg, &ioaddr);

			 if ((ioaddr & PCI_BASE_ADDRESS_IO_MASK) == 0 || (ioaddr
			 & PCI_BASE_ADDRESS_SPACE_IO) == 0)
			 continue;

			 Strip the I/O address out of the returned value 
			 ioaddr &= PCI_BASE_ADDRESS_IO_MASK;

			 Get the memory base address 
			 pcibios_read_config_dword(bus, devfn, PCI_BASE_ADDRESS_1,
			 &membase);

			 Get the ROM base address 
			 pcibios_read_config_dword(bus, devfn, PCI_ROM_ADDRESS, &romaddr);

			 if (ioaddr != 0x0000 || romaddr != 0x0000) {
			 printf("Found at %x, ROM address %x\n", ioaddr, romaddr);
			 //res[pos]= res[pos] + (unsigned long) (ioaddr+romaddr);
			 }
			 }*/

		}
		if (hash_error)
			break;
	}

	if (hash_error)
		return 0;

	return 1;

}

/*int sotp_auth_entry(char *hash, char *passwd, unsigned char *salt, int *res) {
 char *salted_passwd;
 sha1_context ctx;
 unsigned long hashed_passwd[5];
 unsigned long pwlen= grub_strlen(passwd);

 Salt the password 
 salted_passwd = &salted_passwd [ pwlen + SOTP_SALT_SIZE ];
 grub_memmove(salted_passwd, passwd, pwlen);
 grub_memmove( &salted_passwd[pwlen], salt, SOTP_SALT_SIZE);
 pwlen += SOTP_SALT_SIZE;

 Hash the password 
 sha1_init( &ctx);
 sha1_update( &ctx, salted_passwd, pwlen);
 sha1_finish( &ctx, hashed_passwd);

 Compare 
 *res = grub_strcmp(hash, hashed_passwd)==0;

 return 0;
 }
 */

int disk_scan(char *argument, char *result) {
	char *arg = argument;
	char *filename;
	char tmp [1024];
	unsigned long drive;
	unsigned long tmp_drive = saved_drive;
	unsigned long tmp_partition = saved_partition;
	int got_file = 0, next_search = 0, i = 0;

	filename = grub_strcpy(filename, arg);
	grub_printf("Searching %s ...\n", filename);
	for (i=0; i<1024; i++)
		tmp[i] = '\0';

	do {
		next_search = 0;
		for (drive = 0x80; drive < 0x88; drive++) {
			unsigned long part = 0xFFFFFF;
			unsigned long start, len, offset, ext_offset;
			int type, entry;
			char buf[SECTOR_SIZE];

			current_drive = drive;
			while (next_partition(drive, 0xFFFFFF, &part, &type, &start, &len, &offset, &entry,
					&ext_offset, buf)) {
				if (type != PC_SLICE_TYPE_NONE && !IS_PC_SLICE_TYPE_BSD(type)
						&& !IS_PC_SLICE_TYPE_EXTENDED(type)) {
					current_partition = part;
					if (open_device()) {
						saved_drive = current_drive;
						saved_partition = current_partition;
						if (grub_open(filename)) {
							grub_close();
							got_file = 1;
						}
					}
				}
				errnum = ERR_NONE;
			}
			errnum = ERR_NONE;
		}

		saved_drive = tmp_drive;
		saved_partition = tmp_partition;

		if (got_file)
			break;

		if (grub_strstr(filename, "/")) {
			for (i=0; i<strlen(filename) ; i++)
				tmp[i] = filename[i+1];
			grub_printf("Searching for %s ...\n", tmp);
			char *tmp2 = grub_strstr(tmp, "/");
			if (tmp2) {
				next_search = 1;
				for (i=0; i<strlen(filename) ; i++)
					filename[i]='\0';
				filename = grub_strcpy(filename, tmp2);
				grub_printf("Rename to %s ...\n", filename);
			}
		}

	} while (next_search && !got_file);

	if (got_file) {
		grub_printf("Found Filename %s\n", filename);
		result = grub_strcpy(result, filename);
		grub_printf("Found %s\n", result);

		errnum = ERR_NONE;
		return 0;
	}

	grub_printf("Not Found %s\n", filename);
	result = grub_strcpy(result, filename);
	errnum = ERR_FILE_NOT_FOUND;
	return 1;
}

#include "CRC32.h"

#include <stdio.h>
#include <stdarg.h>

include <sys/ioctl.h> //ioctl -> mac-adress
include <net/if.h>
include <unistd.h>
include <sched.h>

#define size_HDDMOD		41
#define size_HDDSN		21
#define size_MACADDR	21
#define size_CPUID		257
#define size_PROC		1

/**
 *  Pour obtenir le nuim�ro de s�rie et le mod�le du disque dur
 */
extern int smartctl (int argc, char **argv);

/**
 * Buffer dans lequel les donn�es de sortie de l'appel � smartctl sont plac�es.
 */
extern char smartctl_PrintBuffer[];

int ParseSmartctl(char *HDDmod, char *HDDsn) {
	char *begin_Token, *end_Token;
	int size;

	//printf("\nParsing de: \n%s\n\n", smartctl_PrintBuffer);

	HDDmod[0] = '\0';
	HDDsn[0] = '\0';

	// On parse le nom du mod�le:
	//printf("On parse le modele: ");
	begin_Token = strstr(smartctl_PrintBuffer, "Device Model:     ");
	if (begin_Token!=NULL) {
		begin_Token += 18;
		end_Token = strchr(begin_Token, '\n');
		size = (int)(end_Token - begin_Token);
		size = (size < 40) ? size : 40;
		strncpy(HDDmod, begin_Token, size);
		//printf("%s", HDDmod);
		if (strncmp(HDDmod, "[No Information Found]", 40)==0)
			HDDmod[0] = '\0';
	}
	//printf("\n");

	// On parse le num�ro de s�rie:
	//printf("On parse le numero de serie: ");
	begin_Token = strstr(smartctl_PrintBuffer, "Serial Number:    ");
	if (begin_Token!=NULL) {
		begin_Token += 18;
		end_Token = strchr(begin_Token, '\n');
		size = (int)(end_Token - begin_Token);
		size = (size < 20) ? size : 20;
		strncpy(HDDsn, begin_Token, size);
		//printf("%s", HDDsn);
		if (strncmp(HDDsn, "[No Information Found]", 20)==0)
			HDDsn[0] = '\0';
	}
	//printf("\n\n\n\n");

	return 0;
}

/**
 * Fonction permettant d'obtenir l'adresse MAC
 */
int GetMACaddress(char *addr) {
	struct ifreq ifr;
	int fd;
	char *ifname[4] = { "eth0", "eth1", "eth2", "eth3" };
	int i=0;

	strcpy(addr, "FF-FF-FF-FF-FF-FF");

	printf("GetMACaddress (%s)\n\n", addr);

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd < 0)
		printf("creating socket failed!\n");

	while (strcmp(addr, "FF-FF-FF-FF-FF-FF")==0 && i<4) {
		strncpy(ifr.ifr_name, ifname[i], IFNAMSIZ);
		memset(&ifr.ifr_hwaddr, 0, sizeof(struct sockaddr));

		if (ioctl(fd, SIOCGIFHWADDR, &ifr)<0) {
			perror(&ifr.ifr_name[0]);
		} else {
			sprintf(addr, "%.2X-%.2X-%.2X-%.2X-%.2X-%.2X",
					ifr.ifr_hwaddr.sa_data[0] & 0xff, ifr.ifr_hwaddr.sa_data[1]
							& 0xff, ifr.ifr_hwaddr.sa_data[2] & 0xff,
					ifr.ifr_hwaddr.sa_data[3] & 0xff, ifr.ifr_hwaddr.sa_data[4]
							& 0xff, ifr.ifr_hwaddr.sa_data[5] & 0xff);
		}

		printf("Interface: %s, MAC = %s\n", ifname[i], addr);

		i++;
	}

	printf("\nGetMACaddress (%s)\n\n\n", addr);

}

#define cpuid(in,a,b,c,d) \
        asm("push %%ebx\n\t" "cpuid\n\t" "mov %%ebx, %%edi\n\t" "pop %%ebx\n\t": "=a" (a), "=D" (b), "=c" (c), "=d" (d) : "a" (in));

void printregs(int eax, int ebx, int ecx, int edx) {
	int j;
	char string[17];

	string[16] = '\0';
	for (j=0; j<4; j++) {
		string[j] = eax >> (8*j);
		string[j+4] = ebx >> (8*j);
		string[j+8] = ecx >> (8*j);
		string[j+12] = edx >> (8*j);
	}
	printf("%s", string);
}

#define VENDOR_UNKNOWN	0
#define VENDOR_INTEL	1
#define VENDOR_AMD		2
#define VENDOR_CYRIX	3

int printCpuInfo() {
	unsigned long max, maxe, a, b, c, d, unused;
	unsigned int vendor = 0; // 0 = Unknown , 1 = Intel, 2 = AMD, 3 = Cyrix
	unsigned long Family_ID;

	cpuid(0,max,b,unused,unused);
	max &= 0xffff; /* The high-order word is non-zero on some Cyrix CPUs */
	for (i=0; i<=max; i++) {
		cpuid(i,a,b,c,d);
		printf("%08x %08lx %08lx %08lx %08lx\n", i, a, b, c, d);
	}

	switch (b) {
	case 0x756e6547: /* Intel */
		vendor = VENDOR_INTEL;
		break;
	case 0x68747541: /* AMD */
		vendor = VENDOR_AMD;
		break;
	case 0x69727943: /* Cyrix */
		vendor = VENDOR_CYRIX;
		break;
	default:
		vendor = VENDOR_UNKNOWN;
		break;
	}

	/* Vendor ID and max CPUID level supported */
	//cpuid(0,unused,b,c,d);
	//printf("Vendor ID: \"");
	//for(i=0;i<4;i++)
	//	putchar(b >> (8*i));
	//for(i=0;i<4;i++)
	//	putchar(d >> (8*i));
	//for(i=0;i<4;i++)
	//	putchar(c >> (8*i));
	//printf("\"; CPUID level %ld\n\n",maxi);

	cpuid(0x80000000,maxe,unused,unused,unused);
	//for(li=0x80000000;li<=maxe;li++)
	//{
	//	cpuid(li,a,b,c,d);
	//	printf("%08lx %08lx %08lx %08lx %08lx\n",li,a,b,c,d);
	//}
	//printf("\n");

	if (maxe >= 0x80000002) {
		/* Processor identification string */
		int j;
		printf("Processor name:\n\n");
		for (j=0x80000002; j<=0x80000004; j++) {
			cpuid(j,a,b,c,d);
			printregs(a, b, c, d);
		}
		printf("\n\n\n");
	} else {
		switch (vendor) {
		case VENDOR_INTEL: /* Intel */
			printf("Processor vendor:\tIntel\n\n");
			break;
		case VENDOR_AMD: /* AMD */
			printf("Processor vendor:\tAMD\n\n");
			break;
		case VENDOR_CYRIX: /* Cyrix */
			printf("Processor vendor:\tCyrix\n\n");
			break;
		default: /* Unknown*/
			printf("Processor vendor:\tUnknown\n\n");
			break;
		}
	}

	if (max >= 1) {
		cpuid(1,a,b,c,d);
		printf("Processor classification:\n");
		printf("CPU Type:\t%d\n", (a & 0x00003000)>>12);

		Family_ID = (a & 0x00000F00)>>8;
		if (Family_ID != 0x0F) // Family_ID
			printf("CPU Family: \t%d\n", Family_ID);
		else
			printf("CPU Family:\t%d\n", ((a & 0x0FF00000)>>20) + Family_ID); // Extended Family ID + Family ID

		if (Family_ID == 0x06 || Family_ID == 0x0F)
			printf("CPU Model:\t%d\n", (((a & 0x000F0000)>>16)<<4) + ((a
					& 0x000000F0)>>4));
		else
			printf("CPU Model:\t%d\n", (a & 0x000000F0)>>4);

		printf("CPU Stepping:\t%d\n", a & 0x0000000F);
		printf("\n\n");

		printf("Processor features:\n");
		if (d & 0x00800000)
			printf("MMX:\tYes\n");
		else
			printf("MMX:\tNo\n");
		if (d & 0x02000000)
			printf("SSE:\tYes\n");
		else
			printf("SSE:\tNo\n");
		if (d & 0x04000000)
			printf("SSE2:\tYes\n");
		else
			printf("SSE2:\tNo\n");
		if (c & 0x00000001)
			printf("SSE3:\tYes\n");
		else
			printf("SSE3:\tNo\n");
		printf("\n\n");
	}

	// restore the affinity setting to its original state
#ifdef WIN32
	SetThreadAffinityMask(GetCurrentThread(), dwProcessAffinity);
	Sleep(0);
#else
	//sched_setaffinity (0, sizeof(allowedCPUs), &allowedCPUs);
	//sleep(0);
#endif
}

int GetCpuID(char *info, unsigned char *size) {
	unsigned long max, maxe, a, b, c, d, unused;
	unsigned char *pos = (unsigned char *)info;
	unsigned int vendor = 0; // 0 = Unknown , 1 = Intel, 2 = AMD, 3 = Cyrix

	cpuid(0,a,b,c,d);
	*((unsigned long *)(pos)) = a; //The largest CPUID standard-function input value
	pos += sizeof(unsigned long);
	*((unsigned long *)(pos)) = b; // Vendor identification string
	pos += sizeof(unsigned long);
	*((unsigned long *)(pos)) = d; // Vendor identification string
	pos += sizeof(unsigned long);
	*((unsigned long *)(pos)) = c; // Vendor identification string
	pos += sizeof(unsigned long);

	switch (b) {
	case 0x756e6547: /* Intel */
		vendor = VENDOR_INTEL;
		break;
	case 0x68747541: /* AMD */
		vendor = VENDOR_AMD;
		break;
	case 0x69727943: /* Cyrix */
		vendor = VENDOR_CYRIX;
		break;
	default:
		vendor = VENDOR_UNKNOWN;
		break;
	}

	max = a&0x0000FFFF; // The high word is not null on some processor (Cyrix).

	if (max>=1) {
		cpuid(1,a,b,c,d);
		*((unsigned long *)(pos)) = a; //Family, Model, Stepping Identifiers = Highest 32 bits of PSN, MASK = & 0x0FFF3FFF
		pos += sizeof(unsigned long);
		*((unsigned long *)(pos)) = b & 0x00FFFFFF; //LogicalProcessorCount, CLFlush, 8BitBrandId
		pos += sizeof(unsigned long);
		*((unsigned long *)(pos)) = d; //Feature Identifiers 2, MASK = & 0xBFEFFBFF
		pos += sizeof(unsigned long);
		*((unsigned long *)(pos)) = c; //Feature Identifiers 1, MASK = & 0x000065B9
		pos += sizeof(unsigned long);
	}
	if (max>=2 && vendor == VENDOR_INTEL) {
		cpuid(2,a,b,c,d);
		*((unsigned long *)(pos)) = a; //Cache and TLB Descriptors
		pos += sizeof(unsigned long);
		if (!(b&0x80000000)) {
			*((unsigned long *)(pos)) = b; //Cache and TLB Descriptors
			pos += sizeof(unsigned long);
		}
		if (!(d&0x80000000)) {
			*((unsigned long *)(pos)) = d; //Cache and TLB Descriptors
			pos += sizeof(unsigned long);
		}
		if (!(c&0x80000000)) {
			*((unsigned long *)(pos)) = c; //Cache and TLB Descriptors
			pos += sizeof(unsigned long);
		}
	}

	if (max>=3 && vendor == VENDOR_INTEL) {
		cpuid(3,unused,unused,c,d);
		*((unsigned long *)(pos)) = d; //Middle 32 bits of PSN
		pos += sizeof(unsigned long);
		*((unsigned long *)(pos)) = c; //Lowest 32 bits of PSN
		pos += sizeof(unsigned long);
	}

	cpuid(0x80000000,a,unused,unused,unused);
	*((unsigned long *)(pos)) = a; //The largest CPUID extended-function input value.
	pos += sizeof(unsigned long);

	maxe = a;

	if (maxe>=0x80000001) {
		cpuid(0x80000001,unused,b,c,d);
		if (vendor == VENDOR_AMD) {
			*((unsigned long *)(pos)) = b;
			pos += sizeof(unsigned long);
			*((unsigned long *)(pos)) = c;
			pos += sizeof(unsigned long);
		}
		*((unsigned long *)(pos)) = d;
		pos += sizeof(unsigned long);
	}

	if (maxe>=0x80000005 && vendor == VENDOR_AMD) {
		cpuid(0x80000005,a,b,c,d);
		*((unsigned long *)(pos)) = a;
		pos += sizeof(unsigned long);
		*((unsigned long *)(pos)) = b;
		pos += sizeof(unsigned long);
		*((unsigned long *)(pos)) = c;
		pos += sizeof(unsigned long);
		*((unsigned long *)(pos)) = d;
		pos += sizeof(unsigned long);
	}

	if (maxe>=0x80000006 && vendor == VENDOR_AMD) {
		cpuid(0x80000006,a,b,c,d);
		*((unsigned long *)(pos)) = a;
		pos += sizeof(unsigned long);
		*((unsigned long *)(pos)) = b;
		pos += sizeof(unsigned long);
		*((unsigned long *)(pos)) = c;
		pos += sizeof(unsigned long);
	}

	*size = (unsigned char)((unsigned char *)pos - (unsigned char *)info);

	return 0;
}

#define HDDMOD_FLAG		1
#define HDDSN_FLAG		2
#define MACADDR_FLAG	4
#define CPUID_FLAG		8

/**
 *  Retourne le buffer du sysId
 *
 *  info buffer permettant de r�cup�rer les informations
 *  size taille du buffer retourner
 *  param checkcode
 *  return 0 si aucun probleme, 1 sinon
 */
int GetBufferID(char** info, unsigned int *size, unsigned int* checkcode,
		int printInfo) {
	char HDDmod[41];
	char HDDsn[21];
	char MACaddr[21];
	char CPUid[257];
	unsigned char sizeCPUid = 0;
	char *param[5];

	memset(HDDmod, '\0', 41);
	memset(HDDsn, '\0', 21);
	memset(MACaddr, '\0', 21);
	memset(CPUid, '\0', 257);

	//printf("On regarde le 1er disque PATA/SATA\n");
	// On tente de trouver le num�ro de s�rie du 1er disque dur PATA/SATA
	param[0] = "smartctl";
	param[1] = "-i";
	param[2] = "-T";
	param[3] = "verypermissive";
	param[4] = "/dev/hda";
	smartctl(5, param);
	ParseSmartctl(HDDmod, HDDsn);

	if (strlen(HDDmod)==0 && strlen(HDDsn)==0) {
		//printf("On regarde le 2�me disque PATA/SATA\n");
		// On tente de trouver le num�ro de s�rie du 2�me disque dur PATA/SATA
		param[0] = "smartctl";
		param[1] = "-i";
		param[2] = "-T";
		param[3] = "verypermissive";
		param[4] = "/dev/hdb";
		smartctl(5, param);
		ParseSmartctl(HDDmod, HDDsn);
	}
	if (strlen(HDDmod)==0 && strlen(HDDsn)==0) {
		//printf("On regarde le 1er disque SCSI\n");
		// On tente de trouver le num�ro de s�rie du 1er disque dur SCSI
		param[0] = "smartctl";
		param[1] = "-i";
		param[2] = "-T";
		param[3] = "verypermissive";
		param[4] = "/dev/sda";
		smartctl(5, param);
		ParseSmartctl(HDDmod, HDDsn);
	}
	if (strlen(HDDmod)==0 && strlen(HDDsn)==0) {
		printf("On regarde le 2�me disque SCSI\n");
		// On tente de trouver le num�ro de s�rie du 2�me disque dur SCSI
		param[0] = "smartctl";
		param[1] = "-i";
		param[2] = "-T";
		param[3] = "verypermissive";
		param[4] = "/dev/sdb";
		smartctl(5, param);
		ParseSmartctl(HDDmod, HDDsn);
	}

	GetMACaddress(MACaddr);

	GetCpuID(CPUid, &sizeCPUid);

	*size = (unsigned int)strlen(HDDmod)+strlen(HDDsn)+strlen(MACaddr)
			+sizeCPUid;
	*info = (char*)malloc(*size);
	**info = '\0';

	if (strlen(HDDmod)!=0) {
		strcat(*info, HDDmod);
		*checkcode += HDDMOD_FLAG;

#ifdef DDEBUG
		if(printInfo>1)
		printf("HDD Model:            %s\n", HDDmod);
#endif
	}

	if (strlen(HDDsn)!=0) {
		strcat(*info, HDDsn);
		*checkcode += HDDSN_FLAG;

#ifdef DDEBUG
		if(printInfo>1)
		printf("HDD Serial Number:    %s\n\n", HDDsn);
#endif
	}

	if (strcmp(MACaddr, "FF-FF-FF-FF-FF-FF")!=0) {
		strcat(*info, MACaddr);
		*checkcode += MACADDR_FLAG;

#ifdef DDEBUG
		if(printInfo>1)
		printf("NIC MAC address:      %s\n\n", MACaddr);
#endif
	}

	if (sizeCPUid!=0) {
		int i;
		int length = (int)(strlen(HDDmod)+strlen(HDDsn)+strlen(MACaddr));
		for (i=0; i<sizeCPUid; i++) {
			(*info)[length+i]=CPUid[i];
		}
		*checkcode += CPUID_FLAG;

		if (printInfo)
			printCpuInfo();
	}

	return 0;
}

int GetSysId(unsigned long *ID, int nb_param, ...) {
	char *info = NULL;
	int printInfo = 0;
	unsigned long checksum = 0xFFFFFFFF;
	unsigned int *checkcode;
	unsigned int chkCode;
	unsigned int *size;

	va_list vl;
	va_start(vl,nb_param);

	if (nb_param>0) {
		checkcode = (unsigned int*)va_arg(vl, unsigned int*);
		*checkcode = 0;
	} else
		checkcode = &chkCode;

	if (nb_param>1)
		printInfo = (int)va_arg(vl, int);

	va_end(vl);

	size = (int*)malloc(sizeof(int));

	GetBufferID(&info, size, checkcode, printInfo);

	if ( *size!=0) {
		*ID = S_A2_F1(&checksum, info, *size);
	}

	if (*ID==checksum) // Seuls les valeurs d'initialisation par d�faut peuvent satisfaire ce test 
	{
		*ID = 0x00000000;
		return 1;
	}

	free(size);

	if (info != NULL)
		free(info);

	return 0;
}

int Padding(char *buf, int bufSize, int FinalSize) {
	int i, nbAppend = FinalSize-bufSize;

	for (i=0; i<nbAppend; i++)
		buf[i+bufSize]=buf[i%bufSize];

	return 0;
}

/**
 *  Retourne le buffer du sysId avec pading pour avoir des tailles fixes
 *
 *   info buffer permettant de r�cup�rer les informations
 *  param size taille du buffer retourner
 *  param checkcode
 *  return 0 si aucun probleme, 1 sinon
 */
int GetFullBufferID(char** info, unsigned int *size, unsigned int* checkcode) {
	char HDDmod[size_HDDMOD];
	char HDDsn[size_HDDSN];
	char MACaddr[size_MACADDR];
	char CPUid[size_CPUID];
	//char Proc[size_PROC];

	unsigned char sizeCPUid = 0;
	char *param[5];

	memset(HDDmod, '\0', size_HDDMOD);
	memset(HDDsn, '\0', size_HDDSN);
	memset(MACaddr, '\0', size_MACADDR);
	memset(CPUid, '\0', size_CPUID);

	//printf("On regarde le 1er disque PATA/SATA\n");
	// On tente de trouver le num�ro de s�rie du 1er disque dur PATA/SATA
	param[0] = "smartctl";
	param[1] = "-i";
	param[2] = "-T";
	param[3] = "verypermissive";
	param[4] = "/dev/hda";
	smartctl(5, param);
	ParseSmartctl(HDDmod, HDDsn);

	if (strlen(HDDmod)==0 && strlen(HDDsn)==0) {
		//printf("On regarde le 2�me disque PATA/SATA\n");
		// On tente de trouver le num�ro de s�rie du 2�me disque dur PATA/SATA
		param[0] = "smartctl";
		param[1] = "-i";
		param[2] = "-T";
		param[3] = "verypermissive";
		param[4] = "/dev/hdb";
		smartctl(5, param);
		ParseSmartctl(HDDmod, HDDsn);
	}
	if (strlen(HDDmod)==0 && strlen(HDDsn)==0) {
		//printf("On regarde le 1er disque SCSI\n");
		// On tente de trouver le num�ro de s�rie du 1er disque dur SCSI
		param[0] = "smartctl";
		param[1] = "-i";
		param[2] = "-T";
		param[3] = "verypermissive";
		param[4] = "/dev/sda";
		smartctl(5, param);
		ParseSmartctl(HDDmod, HDDsn);
	}
	if (strlen(HDDmod)==0 && strlen(HDDsn)==0) {
		//printf("On regarde le 2�me disque SCSI\n");
		// On tente de trouver le num�ro de s�rie du 2�me disque dur SCSI
		param[0] = "smartctl";
		param[1] = "-i";
		param[2] = "-T";
		param[3] = "verypermissive";
		param[4] = "/dev/sdb";
		smartctl(5, param);
		ParseSmartctl(HDDmod, HDDsn);
	}

	GetMACaddress(MACaddr);

	GetCpuID(CPUid, &sizeCPUid);

	Padding(HDDmod, (int)strlen(HDDmod), size_HDDMOD);
	Padding(HDDsn, (int)strlen(HDDsn), size_HDDSN);
	Padding(MACaddr, (int)strlen(MACaddr), size_MACADDR);
	Padding(CPUid, (int)sizeCPUid, size_CPUID);

	*size = size_HDDMOD+ size_HDDSN +size_MACADDR + size_CPUID + size_PROC;
	*info = (char*)malloc(*size);
	**info = '\0';

	memcpy(*info, HDDmod, size_HDDMOD);
	memcpy(&((*info)[size_HDDMOD]), HDDsn, size_HDDSN);
	memcpy(&((*info)[size_HDDMOD+size_HDDSN]), MACaddr, size_MACADDR);
	memcpy(&((*info)[size_HDDMOD+size_HDDSN+size_MACADDR]), CPUid, size_CPUID);

	if (1) {
		(*info)[*size-1]= '1';
	}

	return 0;
}

int GetHWData(char **data, unsigned int* lenght) {
	int printInfo = 0;
	//char *param[5];
	unsigned long checksum = 0xFFFFFFFF;
	unsigned int checkcode;

	GetFullBufferID(data, lenght, &checkcode);

	if ( *lenght==0) {
		return 1;
	}

	return 0;
}

#ifndef CRC32_h
#define CRC32_h

/**
 * Met à jour le CRC32 (checksum) avec les données du buffer.
 *
 *checksum Valeur courante du CRC32, elle doit être initialiser � 0xFFFFFFFF lors du premier appel.
 * buffer Pointeur vers les donn�es � utiliser pour le calcul du CRC32
 * size Taille du buffer
 */
unsigned long S_A2_F1(unsigned long *checksum, char *buffer,unsigned int size);


#endif

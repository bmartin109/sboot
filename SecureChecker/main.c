#include "sha1.h"
#include "check_test.h"


int main(int argc, char *argv[]) {
	int i;
	int c;
	unsigned long hash_result[5];
	sha1_context my_sha1;

	sha1_init(&my_sha1);

	if (argc == 1) {
		fprintf(stderr, "Missing arguments! Usage: %s {filename_list}\n \n", argv[0]);
		return -1;
	}
	
	while ((c = getopt(argc, argv, "h?")) != -1) {
			switch (c) {			
			case 'h':
			case '?':
				fprintf(stderr, "Usage: %s {filename_list}\nCompute System Hashes\n \n", argv[0]);		
				return 0;
			}
		}
	

	if (checkpci_func(&my_sha1)) {
		fprintf(stderr, "Error during PCI Checksum Calculation\n");
		return -1;
	}

	
	if (checkbios_func(&my_sha1)) {
		fprintf(stderr, "Error during BIOS Checksum Calculation\n");
		return -1;
	}

	if (checkfile_func((const char*) argv[1], &my_sha1)) {
		fprintf(stderr, "Error during File Checksum Calculation\n");
		return -1;
	}

	sha1_finish(&my_sha1, hash_result);

	for (i=0; i<5; i++)
		printf("%08lx", hash_result[i]);

	printf("\n");
	return 0;
}


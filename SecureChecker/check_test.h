#ifndef CHECK_TEST_H_
#define CHECK_TEST_H_

#include <pci/pci.h>
#include "param.h"
#include "sha1.h"

int checkfile_func(const char * arg, sha1_context *my_sha1);
int checkpci_func(sha1_context *my_sha1);
int checkbios_func(sha1_context *my_sha1);


//TODO: utilisation de realpath

int checkfile_func(const char * arg, sha1_context *my_sha1) {
	int i;
	int hash_error=0;
	unsigned char sha1_hash_buf_string[41];
	unsigned long hash_input_buf[5];
	char * file_name_buf;
	FILE *fp;

	if (!(file_name_buf = malloc(LINE_BUFFER_SIZE * sizeof(char)))) {
		hash_error=1;
		return 1;
	}

	for (i=0; i<LINE_BUFFER_SIZE; i++)
		file_name_buf[i] = '\0';

	fp=fopen(arg, "r");

	if (!fp) {
		fprintf(stderr, "\nSGRUB error: Cannot open list to check\n");
		fclose(fp);
		return 1;
	}

	while (fgets(file_name_buf, LINE_BUFFER_SIZE, fp) != NULL && !hash_error) {
		char check_file_data[LINE_BUFFER_SIZE];

		if (file_name_buf[0] == '#' || file_name_buf[0] == '\n')
			continue;

		for (i=0; i<LINE_BUFFER_SIZE; i++)
			check_file_data[i]='\0';

		i = 0;
		while (i < strlen(file_name_buf)) {
			if (file_name_buf[i] != '\n')
				check_file_data[i] = file_name_buf[i];
			else {
				check_file_data[i] = '\0';
			}
			i++;
		}

		if (check_file_data[0] == '\0')
			break;

		for (i=0; i<5; i++)
			hash_input_buf[i] = (unsigned long) 0;

#ifdef DEBUG		
		printf("Compute File Integrity %s", file_name_buf);

#endif
		if (calculate_sha1(check_file_data, hash_input_buf)) {
			fprintf(stderr, "\nSGRUB error during SHA1-calculation\n");
			hash_error=1;
			continue;
		}

		for (i=0; i<5; i++) {
			sprintf(&sha1_hash_buf_string[8*i], "%x%x%x%x%x%x%x%x",
					((hash_input_buf[i]>>28)&0x0f), ((hash_input_buf[i]>>24)
							&0x0f), ((hash_input_buf[i]>>20)&0x0f),
					((hash_input_buf[i]>>16) &0x0f), ((hash_input_buf[i]>>12)
							&0x0f), ((hash_input_buf[i]>> 8)&0x0f),
					((hash_input_buf[i]>> 4)&0x0f), ((hash_input_buf[i] )&0x0f));
#ifdef DEBUG	
			printf("%x%x%x%x%x%x%x%x", ((hash_input_buf[i]>>28)&0x0f),
					((hash_input_buf[i]>>24)&0x0f), ((hash_input_buf[i]>>20)&0x0f),
					((hash_input_buf[i]>>16) &0x0f), ((hash_input_buf[i]>>12)&0x0f),
					((hash_input_buf[i]>> 8)&0x0f), ((hash_input_buf[i]>> 4)&0x0f),
					((hash_input_buf[i] )&0x0f));
#endif		
		}
#ifdef DEBUG	
		printf("\n");
#endif		
		sha1_hash_buf_string[40] = '\0';

		if (sha1_update(my_sha1, (unsigned char*) sha1_hash_buf_string, 40)) {
			fprintf(stderr, "\nSGRUB error during SHA1 Hash Update\n");
			hash_error=1;
			continue;
		}

		for (i=0; i<LINE_BUFFER_SIZE; i++)
			file_name_buf[i] = '\0';
	}

	free(file_name_buf);
	fclose(fp);

	if (hash_error)
		return 1;

	return 0;
}

int checkpci_func(sha1_context *my_sha1) {
	struct pci_access *pacc;
	struct pci_dev *dev;
	unsigned int c;
	void * nametab[256];
	int len, hash_error = 0, i = 0, j = 0;
#ifdef DEBUG
	printf("Compute PCI Buses Integrity ...\n");
#endif
	pacc = pci_alloc();

	pci_init(pacc);
	pci_scan_bus(pacc);
	for (dev=pacc->devices; dev; dev=dev->next) {
		void * namebuf;
		pci_fill_info(dev, PCI_FILL_IDENT | PCI_FILL_BASES | PCI_FILL_CLASS);
		c = pci_read_byte(dev, PCI_INTERRUPT_PIN);

		len
				= snprintf(NULL, 0, "%x%02x%x%x%x", dev->bus, dev->dev, dev->func, dev->vendor_id
				& 0xffff, dev->device_id & 0xffff);

		if (!(nametab[i] = (void *) malloc((len + 1) * sizeof(char)))) {
			hash_error=1;
			break;
		}

		namebuf = nametab[i];

		snprintf((char*) nametab[i], len + 1, "%x%02x%x%x%x", dev->bus, dev->dev,
				dev->func, dev->vendor_id & 0xffff, dev->device_id & 0xffff);
		i++;
#ifdef DEBUG	
		printf("%x%02x%x%x%x\n", dev->bus, dev->dev, dev->func, dev->vendor_id
				& 0xffff, dev->device_id & 0xffff);
#endif	
	}

	for (j=i-1; j >= 0; j--) {
		if (sha1_update(my_sha1, (unsigned char *) nametab[j], len)) {
			fprintf(stderr, "\nSGRUB Error during SHA1 Hash Update\n");
			hash_error=1;
			break;
		}
	}

	for (j=0 ; j < i; j++) free(nametab[j]);

	pci_cleanup(pacc);

	if (hash_error)
		return 1;

	return 0;
}

int checkbios_func(sha1_context *my_sha1) {

	int fd, bytes_to_copy= BIOS_ROM_SIZE;
	char *buf;
	unsigned char *pos;
	int i, j;
	int hash_error=0;
	unsigned long Polynomial = 0xEDB88320;
	unsigned long Crc32Table[256];
	unsigned long checksum = 0xFFFFFFFF;
	unsigned long Crc;
	unsigned char Crc32_hash_buf_string[8];

	fd = open(MEM_DEV, O_RDONLY);

	if (fd < 0) {
		fprintf(stderr, "\nSGRUB Error during opening '/dev/mem'\n");
		perror("");
		return 1;
	}

	if (!(buf = malloc(bytes_to_copy * sizeof(char)))) {
		fprintf(stderr, "\nSGRUB Error during memory allocation\n");
		close(fd);
		return 1;
	}

	pos = (unsigned char*) buf;

#ifdef DEBUG
	printf("Compute BIOS Checksum ...\n");
#endif

	for (i = 0; i < 256; i++) {
		Crc = i;
		for (j = 8; j > 0; j--) {
			if (Crc & 1)
				Crc = (Crc >> 1) ^ Polynomial;
			else
				Crc >>= 1;
		}
		Crc32Table[i] = Crc;
	}

	if (!lseek(fd, BIOS_ROM_START, SEEK_SET)) {
		fprintf(stderr, "\nSGRUB Error during positioning memory record\n");
		return 1;
	}

	/*while (bytes_to_copy > BUF_SIZE) {

	 read(fd, buf, BUF_SIZE)
	 
	 pos = (unsigned char*) buf;
	 for (i = 0; i<BUF_SIZE; i++) {
	 //		checksum = ((checksum) >> 8)^Crc32Table[(*pos)^((checksum) & 0x000000FF)];
	 printf("%c",*pos);
	 pos++;
	 }
	 
	 if (!lseek(fd, BUF_SIZE, SEEK_CUR)) {
	 printf("\nSGRUB Error during positioning memory record\n");
	 hash_error=1;
	 break;
	 }
	 bytes_to_copy = bytes_to_copy - BUF_SIZE;
	 }
	 
	 if (hash_error) {
	 close(fd);
	 free(buf);
	 return 1;
	 }*/

	//pos = (unsigned char*) buf;
	memset(buf, 0, bytes_to_copy);
	read(fd, buf, bytes_to_copy);
	for (i = 0; i<bytes_to_copy; i++) {
		checksum = ((checksum) >> 8)^Crc32Table[(*pos)^((checksum) & 0x000000FF)];
		pos++;
	}

	close(fd);
	free(buf);

	sprintf(&Crc32_hash_buf_string[0], "%x%x%x%x%x%x%x%x",
			((checksum>>28)&0x0f), ((checksum >> 24) &0x0f), ((checksum >> 20)
					&0x0f), ((checksum >> 16) &0x0f), ((checksum >> 12)&0x0f),
			((checksum >> 8)&0x0f), ((checksum >> 4)&0x0f), ((checksum)&0x0f));
	Crc32_hash_buf_string[8] = '\0';

#ifdef DEBUG	
	printf("%x%x%x%x%x%x%x%x\n", ((checksum>>28)&0x0f),
			((checksum>>24)&0x0f), ((checksum>>20)&0x0f),
			((checksum>>16) &0x0f), ((checksum>>12)&0x0f),
			((checksum>> 8)&0x0f), ((checksum>> 4)&0x0f),
			((checksum )&0x0f));
#endif		

	if (sha1_update(my_sha1, (unsigned char *) Crc32_hash_buf_string, 8)) {
		fprintf(stderr, "\nSGRUB Error during SHA1 Hash Update\n");
		hash_error=1;
		return 1;
	}

	return 0;
}

#endif /*CHECK_TEST_H_*/

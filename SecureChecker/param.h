#ifndef PARAM_H_
#define PARAM_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#define BIOS_ROM_START  0xf0000
#define BIOS_ROM_SIZE   0x10000
#define BUF_SIZE			512
#define LINE_BUFFER_SIZE 	1024
#define MEM_DEV 		"/dev/mem"

#endif /*PARAM_H_*/

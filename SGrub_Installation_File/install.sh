#! /bin/sh

# Initialize some variables.
path=$(pwd)
hdir='/home'
help="Usage: $0 [options] [--check-file FILE] [--account-size SIZE] [--password PASSWORD] LOGIN"
pwfile="/etc/passwd"
shadowfile='/etc/shadow'
gfile='/etc/group'
pammntfile="/etc/security/pam_mount.conf"
pamkdefile="/etc/pam.d/login"
pammodule="/lib/security/pam_sgrub.so"
sbootinst="${path}/tgrub/sbin/grub-install"
sbootconfig="/boot/grub/checklist.conf"
checkconfig="/boot/grub/checklist"
checkinst="${path}/securecheck"
checkconverter="${path}/sconverter"
checkmodule="${path}/pam_sgrub.so"
grubconfig="/boot/grub/menu.lst"
oldgrubconfig="/boot/grub/menu.lst.old"
newgrubconfig="/boot/grub/menu.lst.new"
keyfile="/mnt/usb/key"
mntfile="/etc/fstab"
shell="/bin/bash"
device="/dev/sda"
krnlentry=""
result=""

# Validate user environment
if [ "$(whoami)" != "root" ] ; then
  echo "Error: You must be root to run this command." >&2
  exit 1
fi

if [ ! -x $sbootinst ]; then
  echo "Error: You must install ${sbootinst}" >&2
  exit 1
fi

result=$(awk -F= '$1=="prefix" {print $2;}' ${sbootinst});
if [ "${result}" != "$(pwd)/tgrub" ]; then
  echo "" >&2
  echo "Error: ${sbootinst}" >&2
  echo "Wrong 'prefix' parameter" >&2
  echo "You must specify path $(pwd)/tgrub" >&2
  echo "" >&2
  exit 1
fi

if [ ! -x $checkinst ]; then
  echo "Error: You must install ${checkinst}" >&2
  exit 1
fi

if [ ! -x $checkconverter ]; then
  echo "Error: You must install ${checkconverter}" >&2
  exit 1
fi

if [ ! -f $checkmodule ]; then
  echo "Error: You must install ${checkmodule}" >&2
  exit 1
fi

if [ ! -r $pamkdefile ]; then
  echo "Error: Missing or wrong file ${pamkdefile}" >&2
  exit 1
fi

# Parsing user arguments
while [ $# -gt 0 ]
do
  case $1
  in
	--help | -h        )
echo $help >&2
echo "" >&2
echo "Options:" >&2
#echo "	 -b, --base-dir BASE_DIR        Base directory for the new user account" >&2
echo "	 --boot-kernel TITLE            Set title entry to modify from menu.lst [default entry]" >&2
echo "	 --config-file FILE             Specify Stage2 config_file [default=/boot/grub/menu.lst]" >&2
#echo "-c, --comment COMMENT         set the GECOS field for the new user account" >&2
echo "	 -d, --home-dir HOME_DIR        Home directory for the new user account [default=/home/LOGIN]" >&2
#echo "-D, --defaults                print or save modified default useradd" >&2
#echo "                              configuration" >&2
#echo "-e, --expiredate EXPIRE_DATE  set account expiration date to EXPIRE_DATE" >&2
#echo "-f, --inactive INACTIVE       set password inactive after expiration" >&2
#echo "                              to INACTIVE" >&2
#echo "-g, --gid GROUP               force use GROUP for the new user account" >&2
#echo "-G, --groups GROUPS           list of supplementary groups for the new" >&2
#echo "                              user account" >&2
echo "	 -h, --help                     Display this help message and exit" >&2
echo "	 -i, --install_device		Specify GRUB device name to install secure GRUB [default=${device}]" >&2
#echo "-k, --skel SKEL_DIR           specify an alternative skel directory" >&2
#echo "-K, --key KEY=VALUE           overrides /etc/login.defs defaults" >&2
echo "	 -l, --check-file FILE          Specify checking filename list path" >&2
#echo "-m, --create-home             create home directory for the new user" >&2
#echo "                              account" >&2
#echo "-M,                   do not create home directory for the new user" >&2
#echo "-n,                   do not create a group with the same name" >&2
#echo "                              as the user" >&2
#echo "-o, --non-unique              allow create user with duplicate" >&2
#echo "                              (non-unique) UID" >&2
echo "	 -p, --password PASSWORD        Specify Encrypted Password for the new user" >&2
#echo "                              account" >&2
#echo "-r                            create a system account" >&2
echo "	 -s, --shell SHELL              Login Shell for the new user account [default=/bin/bash]" >&2
#echo "-u, --uid UID                 force use the UID for the new user account" >&2
echo "	 -z, --account-size SIZE        Set Account Size (in MB) for the new user" >&2
                       exit 1 ;;
#	--base-dir | -b) 	hdir=$2;       shift ;;
	--boot-kernel)		krnlentry=$2;	shift 2 ;;
	--config-file)		grubconfig=$2;	shift 2 ;;
	--home-dir | -d)	homedir=$2;	shift 2 ;;
	--install_device | -i)	device=$2;	shift 2 ;;
	--check-file | -l)	checkfile=$2;	shift 2 ;;
	--password | -p)  	passwd=$2;       shift 2 ;;
	--shell | -s)  		shell=$2;   shift 2 ;;	
	--account-size | -z)  	size=$2;   shift 2 ;;	
	--              )  shift;           break ;;
	*	)	break;	# done with 'while' loop!
  esac
done

if [ -z $checkfile ]; then
	echo "Error: Wrong or missing arguments '--check-file'  ${checkfile}" >&2;
	echo $help >&2
	echo "" >&2
	exit 1;
elif [ ! -r ${checkfile} ]; then 
	echo "Error: ${checkfile}  Wrong or missing file" >&2;
	exit 1;
fi

if [ -z $size ]; then
	echo "Error: Wrong or missing arguments '--account-size'  ${size}" >&2;	
	echo $help >&2
	echo "" >&2
	exit 1;
fi

if [ -z $passwd ]; then
	echo "Error: Wrong or missing arguments '--password'  ${passwd}" >&2;	
	echo $help >&2
	echo "" >&2
	exit 1;
fi

if [ ! -r $device ]; then
	echo "Error: Wrong or missing arguments '--install_device'  ${passwd}" >&2;	
	echo $help >&2
	echo "" >&2
	exit 1;
fi

if [ ! -r $grubconfig ]; then
	echo "Error: ${grubconfig} Wrong or missing file" >&2;
	echo "";
	exit 1;	
fi

if [ -z "$1" ]; then
	echo "Error: You must specify LOGIN username  ${login}" >&2;	
	echo $help >&2
	echo "" >&2
	exit 1;
else
	login="$1";
fi

result=$(awk -F: '{print $1 }' ${pwfile} | grep -w ${login});
if [ ! -z $result ]; then
	echo "Error: User name '${login}' already exist" >&2;	
	echo "Please enter another user name"
	echo "" >&2
	exit 1;
fi

if [ -z $homedir ]; then	
	homedir="$hdir/$login";
#	echo "";
#	echo "Choosing home directory by default: ${homedir}" >&2;
#	echo "";
fi

if [ -z $krnlentry ]; then
	result=$(grep -c check ${grubconfig});
	if [ $result -eq 0 ]; then
		krnltitle=$(awk '$1=="default" {res=$2} END {print res}' ${grubconfig});
		krnline=$(grep -n title ${grubconfig} | awk -F: '{if ('${krnltitle}'==cpt) {print $1}; cpt=cpt+1;}' );
		if [ -z $krnline ]; then	
			echo "Error: Corrupted or wrong ${grubconfig} file" >&2;
			exit 1
		fi
	fi
	else
	result=$(grep -c "title ${krnlentry}" ${grubconfig});
	if [ $result -eq 1 ]; then
		krnline=$(grep -n "title ${krnlentry}" ${grubconfig} | awk -F: '{print $1}' );
		if [ -z $krnline ]; then	
			echo "Error: Kernel Entry not found in ${grubconfig} file" >&2;
			exit 1
		fi
	else
		echo "Error: Multiple Entries ${krnlentry} into ${grubconfig}" >&2 ;
		echo "Precise the kernel title you want to check" >&2 ;
		echo "";
		exit;
	fi
fi

mount /mnt/usb 2> /dev/null
if [ ! -f $keyfile ]; then
 echo "Error: Mounting USB Protector Key." >&2;
 echo "Please check if USB key is correctly  mounted on '/mnt/usb'" >&2;
 echo "";
 exit 1
fi

# echo "Add new User Account to $(hostname)"
# echo -n "Login: "     ; read login

# uid="$(awk -F: '{ if (big < $3 && $3 < 5000) big=$3 } END { print big + 1 }' $pwfile)"
# homedir=$hdir/$login
imgfile=$hdir/$login.img
userkeyfile="${keyfile}_${login}"
masterkeyfile="${keyfile}_${login}.original"
# We are giving each user their own group, so gid=uid
# gid=$uid

# echo -n "Full name: " ; read fullname
# echo -n "Shell: "     ; read shell
# echo -n "Password: "     ; read passwd
# echo -n "Account Size (in MB): " ; read size
# echo -n "Enter files list to check: " ; read fileconf

${checkconverter} -l ${checkfile} -o ${sbootconfig} 2&> $result;
if grep "file" ${result} >/dev/null; then
 echo "Error: ${checkconverter} -l ${checkfile} -o ${sbootconfig}" >&2;
 cat ${result} >&2;
 echo "" >&2;
 exit 1
fi


echo "";
echo "Setting up new Secured Grub ..."
echo "";
${sbootinst} --recheck ${device};
echo "";

check_id="$(${checkinst} ${checkfile})" 

# if grep "error" $check_id >/dev/null; then
# echo "Error: ${checkinst} $checkfile" >&2;
# cat "${result}"; >&2;
# exit 1
# fi

cp -f ${checkfile} ${checkconfig}

passphrase="${check_id}${passwd}"

echo "";
echo "Setting up account ${login} ..."
echo "";

# echo ${login}:x:${uid}:${gid}:${fullname}:${homedir}:$shell >> $pwfile
# echo ${login}:*:11647:0:99999:7::: >> $shadowfile
# echo "${login}:x:${gid}:$login" >> $gfile

mkdir $homedir  2>/dev/null
chmod 755 $homedir

dd if=/dev/urandom of=$imgfile bs=1M count=$size 2>/dev/null

# Setting an key passphrase on USB
if test -f "$userkeyfile"; then
    rm -f $userkeyfile
fi

/sbin/modprobe -q cryptoloop
dd if=/dev/random bs=1c count=8 2>/dev/null | openssl enc -aes-256-ecb -pass pass:$passphrase > $userkeyfile
openssl enc -d -aes-256-ecb -pass pass:$passphrase -in $userkeyfile | openssl enc -aes-256-ecb -pass pass:$passwd > $masterkeyfile
openssl enc -d -aes-256-ecb -pass pass:$passphrase -in $userkeyfile | /sbin/losetup -e aes -p0 /dev/loop0 $imgfile
/sbin/mkfs -t ext2 /dev/loop0
mount -t ext2 /dev/loop0 $homedir
# cp -R /etc/skel/.[a-z]* $homedir
# 
useradd -m -p $passwd $login
touch $homedir/hello.txt
echo "Hello World!" > $homedir/hello.txt
#find $homedir -print | xargs chown ${login}:${login}
chown -R ${login}:${login} $homedir
chown ${login}:${login} $homedir
umount $homedir
/sbin/losetup -d /dev/loop0

echo "";
echo "Generating System Checksum Integrity for $login account..."
echo "";

# Setting user password
echo "${passwd}" | passwd $login --stdin > /dev/null


# Setting tGrub PAM module
if [ ! -f "$pammodule" ]; then
      cp $checkmodule $pammodule
fi

# Setting new configuration
if [ ! -z $krnline ]; then
	nbline=$(wc -l ${grubconfig} | awk '{print $1}')
	head -n $krnline ${grubconfig} > $newgrubconfig
	printf "checkpci \ncheckbios \ncheckfile %s \n" $(${checkconverter} ${sbootconfig}) >> $newgrubconfig
	tail -n $(($nbline-$krnline)) ${grubconfig} >> $newgrubconfig
	mv $grubconfig $oldgrubconfig
	mv $newgrubconfig $grubconfig
	rm -f $newgrubconfig
	chmod 664 $grubconfig
fi

#echo "<volume user="${login}" path="${imgfile}" fskeycipher="aes-256-ecb" fskeypath="${userkeyfile}" />" >> ${pammntfile}.xml
echo "volume ${login} auto - ${imgfile} - - aes-256-ecb ${userkeyfile}" >> $pammntfile
/usr/sbin/convert_pam_mount_conf.pl -i$pammntfile -o${pammntfile}.xml
rm -f $pammntfile
echo  "${imgfile} ${homedir} ext2  user,loop,encryption=aes,noauto 0 0" >> $mntfile


echo ""
echo "*******************************************************************"
echo " Account ${login} has been added"
echo ""
echo " Home directory ($homedir), partition data (${imgfile})"
echo " and protected USB Key (${userkeyfile}) have been created."
echo ""
echo ""
echo " YOU MUST ADD FOLLOWING LINES INTO YOUR PAM CONFIGURATION TO USE"
echo " THIS USER ACCOUNT:"
echo ""
echo "	auth       required		pam_sgrub.so"
echo "	auth	   required		pam_mount.so use_first_pass"
echo "	session    required		pam_mount.so"
echo ""
echo "*******************************************************************"
echo ""

exit 0

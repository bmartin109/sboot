#!/bin/sh

# Initialize some variables.
path=$(pwd)
checkinst="${path}/securecheck"
help="Usage: $0  [--password PASSWORD] LOGIN"
pwfile="/etc/passwd"
sbootconfig="/boot/grub/checklist.conf"
checkconfig="/boot/grub/checklist"
keyfile="/mnt/usb/key"


if [ -z $1 ] ; then
  echo $help >&2; exit 1
elif [ "$(whoami)" != "root" ] ; then
  echo "Error: you must be 'root' to run this command.">&2; exit 1
fi

if [ ! -x $checkinst ]; then
  echo "Error: You must install ${checkinst}" >&2
  exit 1
fi

if [ ! -r $sbootconfig ]; then
  echo "Error: Missing or wrong file ${sbootconfig}" >&2
  exit 1
fi

if [ ! -r $checkconfig ]; then
  echo "Error: Missing or wrong file ${checkconfig}" >&2
  exit 1
fi

while [ $# -gt 0 ]
do
  case $1
  in
	--help | -h        	)	echo $help >&2;  exit 1 ;;	
	--password | -p		)  	passwd=$2;       shift 2 ;;
	--              )  shift;           break ;;
	*	)	break;	# done with 'while' loop!
  esac
done

# verify account integrity
uid="$(grep -E "^${1}:" $pwfile | cut -d: -f3)"
if [ -z $uid ] ; then
 echo $help >&2;  
 echo "Error: no account $1 found in '$pwfile'" >&2; exit 1
fi

mount /mnt/usb 2> /dev/null
userkeyfile="${keyfile}_${1}"
masterkeyfile="${keyfile}_${1}.original"
if [ ! -r $masterkeyfile ]; then
 echo "Error: No protection key available for ${1} account" >&2;
 echo "Please check if USB key is correctly mounted on '/mnt/usb'" >&2; exit 1
fi

check_id="$(${checkinst} ${checkconfig})" 
passphrase="${check_id}${passwd}"
result=$(openssl enc -d -aes-256-ecb -in ${masterkeyfile} -pass pass:${passwd} | openssl enc -aes-256-ecb -pass pass:${passphrase} > ${userkeyfile})

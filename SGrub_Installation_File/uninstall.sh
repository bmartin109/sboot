#!/bin/sh

# Initialize some variables.
homedir="/home"
pwfile="/etc/passwd"
shadow="/etc/shadow"
gfile="/etc/group"
mntfile="/etc/fstab"
newpwfile="/etc/passwd.new"
newshadow="/etc/shadow.new"
newgfile="/etc/group.new"
newmntfile="/etc/fstab.new"
pammntfile="/etc/security/pam_mount.conf.xml"
newpammntfile="/etc/security/pam_mount.conf.xml.new"
locker="/etc/passwd.lock"
keyfile="/mnt/usb/key"

if [ -z $1 ] ; then
  echo "Usage: $0 account" >&2; exit 1
elif [ "$(whoami)" != "root" ] ; then
  echo "Error: you must be 'root' to run this command.">&2; exit 1
fi

# verify account integrity
uid="$(grep -E "^${1}:" $pwfile | cut -d: -f3)"
if [ -z $uid ] ; then
 echo "Error: no account $1 found in '$pwfile'" >&2; exit 1
fi

mount /mnt/usb  2> /dev/null
userkeyfile="${keyfile}_${1}"
masterkeyfile="${keyfile}_${1}.original"
if [ ! -r $userkeyfile ]; then
 echo "Error: No protection key available for $1 account" >&2;
 echo "Please check if USB key is correctly mounted on '/mnt/usb'" >&2; exit 1
fi

imgfile=$homedir/${1}.img
if [ ! -r $imgfile ]; then
 echo "Error: no user partition available for $1 account" >&2; exit 1
fi

echo "Remove from the password files ..."
grep -vE "^${1}:" $pwfile > $newpwfile
grep -vE "^${1}:" $shadow > $newshadow
grep -vE "^${1}:" $gfile > $newgfile

echo "Remove Secure Protection for $1 account ..."
grep -vE "${userkeyfile}" $pammntfile > $newpammntfile
grep -vE "^${imgfile}" $mntfile > $newmntfile

# created a file-based lock
while [ -e $locker ] ; do
    echo "Waiting for the password file" ; sleep 1
  done
  touch $locker

mv $newpwfile $pwfile
mv $newshadow $shadow
mv $newgfile $gfile
mv $newpammntfile $pammntfile
mv $newmntfile $mntfile
rm -f $locker

chmod 644 $pwfile
chmod 400 $shadow
chmod 644 $gfile
chmod 644 $pammntfile
chmod 644 $mntfile

rm -rf $homedir/$1
rm -f $userkeyfile
rm -f $masterkeyfile
rm -f $imgfile

echo ""
echo "*****************************************************************"
echo " Account $1 (uid $uid) has been deleted"
echo ""
echo " Home directory ($homedir/$1), partition data ($imgfile)"
echo " and protected USB Key ($userkeyfile) have been removed."
echo ""
echo "*****************************************************************"
exit 0

# About #

SBoot provides cryptographic features to ensure hardware and software integrity of a PC Platform running with Linux OS. It is an extension for the standard GRUB bootloader.

SBoot is developed in the scope of an ITEA project: E-Confidential.


### Description ###

The goal of the project is to secure access to user data partitions on a PC Linux. The security is based on a external container stored on an USB disk storage.

Access Control to the user data partition is based on 2 processes:

-   System Integrity: secured bootloader checks principal components of PC Platform: CPU-ID, NIC-ID, HDD-ID, PCI-ID, BIOS, /boot partition, MBR, kernel and important system files (ex: /etc/security/\*, /etc/sysctl.conf, ...)

-   User Authentification: secured bootloader allow mounting crypted user partition after successfully checking system integrity and extracting key contained on USB Token via login.

The implementation is compatible with different encryption methods: eCryptFS, dmcrypt and TrueCrypt.


### Installation ###

* Summary

The basic elements of the system are the following :

- a modified grub 1.0.1

- a usb key with the certificate used to encrypt the user partition. The passphrase to access the certificate is built from the user password and a hardware and software signature worked out by the modified grub

- an encrypted user partition

The system is working in the following way :

1.  On boot, a modified grub is working out a bios signature, a hardware signature (all pciids, ram, hard disk size), and then a number of file signature.

2.  You choose the files on which you do a signature, linux is then normally booting. When arriving at logging, the usb key (in the current version) must have been plugged in.

3.  pam_mount uses the previously grub hardware/software signature and the user password as the passphrase for getting the encryption certificate. 

* How to set up

Instructions for building and installing Secure boot are provided in the Installation Guide.txt file.

* Configuration

3 scripts to set up the system:

*install.sh* : set the secure bootloader and an new user account. It recommends an new PAM configuration to use the new account..

*uninstall.sh* : remove the specified user account. Warning: it doesn't uninstall secure Grub and destroy all user data.

*update.sh* : regenerate key locker on USB key.

* Dependencies

libpam 1.0.1


### Implementation ###

* Design

Secure Boot relies on the use of an safe USB key.

At the boot sequence, after authenticating USB key, it retrieve user's checking commands stored on the external device. It checks and validates system platform integrity. And then, Secure Boot delivers the system authenticity agreement which will be used by the login manager in Linux environment.

The login manager supports 2 modes: Standard and Update modes. 
Standard mode is used by users for user and system authentication.
Update mode is used by the manager to recomputes system integrity to take into account hardware or software modifications.


* Other guidelines

### Reference documents ###

[Inside the Linux boot process](http://www.ibm.com/developerworks/library/l-linuxboot/index.html)
 by Tim Jones

[Trusted Computing Group](https://www.trustedcomputinggroup.org/)

<http://www.sirrix.com/content/pages/trustedgrub.htm>

[BIOS. Enhanced Disk Drive Specification. Version 3.0](http://www.phoenix.com/NR/rdonlyres/19FEBD17-DB40-413C-A0B1-1F3F560E222F/0/specsedd30.pdf)

[AMD CPUID Specification](http://www.amd.com/us-en/assets/content_type/white_papers_and_tech_docs/25481.pdf)

[Intel® Processor Identification and the CPUID Instruction Application Note 485](http://download.intel.com/design/processor/applnots/24161832.pdf)

[Conventional PCI Local Bus Specification 3.0](http://www.pcisig.com/members/downloads/specifications/conventional/PCI_LB3.0-2-6-04.pdf)

[PKCS \#5 v2.0 Password-Based Cryptography Standard](ftp://ftp.rsasecurity.com/pub/pkcs/pkcs-5v2/pkcs5v2_1.pdf)

[IMA Measurement Example](http://domino.research.ibm.com/comm/research_projects.nsf/pages/ssd_ima.measurements.html)